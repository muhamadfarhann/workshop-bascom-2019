<h3>Dashboard</h3>
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-green"><i class="fa fa-user-o"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">Peserta</span>
				<span class="info-box-number"><?= $peserta; ?> Peserta</span>
				<a href="<?php echo base_url('peserta/index');?>">Lihat Data</a>
			</div>
			<!-- /.info-box-content -->
		</div>
		<!-- /.info-box -->
	</div>	
</div>