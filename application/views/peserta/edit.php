<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Peserta Edit</h3>
            </div>
			<?php echo form_open('peserta/edit/'.$peserta['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="kelas" class="control-label"><span class="text-danger">*</span>Kelas</label>
						<div class="form-group">
							<select name="kelas" class="form-control">
								<option value="" selected disabled>- Pilih Kelas -</option>
								<option value="3IF-1">3IF-1</option>
								<option value="3IF-2">3IF-2</option>
								<option value="3IF-3">3IF-3</option>
								<option value="3SI-1">3SI-1</option>
								<option value="3SI-2">3SI-2</option>
								<option value="3MD-1">3MD-1</option>
								<option value="3AB-1">3AB-1</option>
								<option value="3AB-2">3AB-2</option>
								<option value="3KA-1">3KA-1</option>
								<option value="3KA-2">3KA-2</option>
								<option value="3KU-1">3KU-1</option>
								<option value="3KU-2">3KU-2</option>
								<option value="4IF-1">4IF-1</option>
								<option value="4IF-2">4IF-2</option>
								<option value="4SI-1">4SI-1</option>
								<option value="4SI-2">4SI-2</option>
								<?php 
								$kelas_values = array(
								);

								foreach($kelas_values as $value => $display_text)
								{
									$selected = ($value == $peserta['kelas']) ? ' selected="selected"' : "";

									echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('kelas');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="nama" class="control-label"><span class="text-danger">*</span>Nama</label>
						<div class="form-group">
							<input type="text" name="nama" value="<?php echo ($this->input->post('nama') ? $this->input->post('nama') : $peserta['nama']); ?>" class="form-control" id="nama" />
							<span class="text-danger"><?php echo form_error('nama');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="nrp" class="control-label"><span class="text-danger">*</span>Nrp</label>
						<div class="form-group">
							<input type="text" name="nrp" value="<?php echo ($this->input->post('nrp') ? $this->input->post('nrp') : $peserta['nrp']); ?>" class="form-control" id="nrp" />
							<span class="text-danger"><?php echo form_error('nrp');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="no_telepon" class="control-label"><span class="text-danger">*</span>No Telepon</label>
						<div class="form-group">
							<input type="text" name="no_telepon" value="<?php echo ($this->input->post('no_telepon') ? $this->input->post('no_telepon') : $peserta['no_telepon']); ?>" class="form-control" id="no_telepon" />
							<span class="text-danger"><?php echo form_error('no_telepon');?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>