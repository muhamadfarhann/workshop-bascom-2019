<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Daftar Peserta Workshop</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('peserta/add'); ?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Tambah Data</a> 
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-striped">
                    <tr>
						<th>No</th>
						<th>Nama</th>
                        <th>Kelas</th>
						<th>NRP</th>
						<th>No Telepon</th>
						<!-- <th>Actions</th> -->
                    </tr>
                    <?php foreach($peserta as $p){ ?>
                    <tr>
						<td><?php echo $p['id']; ?></td>
						<td><?php echo $p['nama']; ?></td>
                        <td><?php echo $p['kelas']; ?></td>
						<td><?php echo $p['nrp']; ?></td>
						<td><?php echo $p['no_telepon']; ?></td>
						<!-- <td>
                            <a href="<?php echo site_url('peserta/edit/'.$p['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('peserta/remove/'.$p['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td> -->
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
        <a href="<?php echo base_url('peserta/pdf'); ?>" class="btn btn-danger"><i class="fa fa-print"></i> PDF</a>
        <a href="<?php echo base_url('peserta/excel'); ?>" class="btn btn-warning"><i class="fa fa-file"></i> Excel</a>  
    </div>
</div>
